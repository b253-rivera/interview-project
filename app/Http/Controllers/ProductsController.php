<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::where('user_id', auth()->user()->id)->get();
        
        return Inertia::render('Products/Index', [
            'products' => $products
        ]);
    }
    public function create()
    {
        return Inertia::render('Products/CreateProduct');
    }

    public function store(Request $request)
    {
        Product::query()->create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'user_id' => auth()->user()->id,
        ]);

        return redirect()->back()->with('success', 'Product created successfully!');
    }

    public function edit($id)
    {
        $products = Product::findOrFail($id);
        return Inertia::render('Products/UpdateProduct', ['products' => $products]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
        ]);
        return redirect()->back()->with('success', 'Product updated successfully!');
    }
}